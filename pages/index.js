import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <>
    <Head>
      <title>Song List | Home</title>
      <meta name='keywords' content='song' />
    </Head>
    <div>
      <h1 className={styles.title}>Homepage</h1>
      <p className={styles.text}>Hello Everyone</p>
      <Link href="/ninjas">
      <a className={styles.btn}>Ninjas Listing</a></Link>
      <Image src='/vercel.svg' style={{ width: 30, height: 50 }} alt='vercel' />
    </div>
    </>
  )
}
