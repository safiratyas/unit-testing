const Dog = require('./Dog');

describe('Dog', () => {
  it("should have name called 'Mowwy'", () => {
    const dog = new Dog("Mowwy");

    expect(dog).toHaveProperty("name", "Mowwy");
  });

  it("should be able to bark and return 'Woof!'", () => {
    const dog = new Dog("Mowwy");
    expect(dog.bark()).toEqual("Woof!");
  });
});
